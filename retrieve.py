#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) 2018 by Mathieu Servillat
# Licensed under MIT
"""
UWS client implementation using bottle.py and javascript
"""

import os
import logging
import logging.config
from flask import Flask, request, send_from_directory, render_template, abort


# ----------
# Configuration

# App configuration
#DEBUG=False
#TESTING=False
#SERVER_NAME=  # (e.g.: 'myapp.dev:5000')
APP_PATH = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))
VAR_PATH = '/var/www/retrieve'
DATA_PATH = VAR_PATH + '/data'
LOG_PATH = VAR_PATH + '/logs'  # the logs dir has to be writable from the app

# Set logger
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'default': {
            'format': '[%(asctime)s] %(levelname)s %(funcName)s: %(message)s'
        },
    },
    'handlers': {
        'file': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': LOG_PATH + '/retrieve.log',
            'formatter': 'default'
        },
    },
    'loggers': {
        'retrieve': {
            'handlers': ['file'],
            'level': 'DEBUG',
        },
    }
}

# Create var dirs if not exist
for p in [VAR_PATH + '/logs']:
    if not os.path.isdir(p):
        os.makedirs(p)

# Set logger
logging.config.dictConfig(LOGGING)
logger = logging.getLogger('retrieve')


# ----------
# create the application instance :)


app = Flask(__name__, instance_relative_config=True)  # , instance_path=VAR_PATH)
app.secret_key = b'\ttrLu\xdd\xde\x9f\xd2}\xc1\x0e\xb6\xe6}\x95\xc6\xb1\x8f\xa09\xf5\x1aG'
#app.config.update(EDITABLE_CONFIG)  # Default editable config
app.config.from_object(__name__)  # load config from this file

error_text_404 = '''
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<title>404 Not Found</title>
<h1>Not Found</h1>
<p>The requested file was not found on the server.</p>
'''


# ----------
# Web Pages


@app.errorhandler(404)
def page_not_found(e):
    # note that we set the 404 status explicitly
    return error_text_404, 404


@app.route('/', methods=["GET"])
def home():
    #return 'Home'
    return render_template('home.html')


@app.route('/retrieve/<filename>', methods=["GET"])
def retrieve(filename):
    """Home page"""
    if os.path.isfile(os.path.join(app.config['DATA_PATH'], filename)):
        logger.info('Downloading: {} from {}'.format(filename, request.remote_addr))
        return send_from_directory(app.config['DATA_PATH'], filename)
    else:
        logger.warning('File not found: {} from {}'.format(filename, request.remote_addr))
        abort(404)


# ----------
# run server


if __name__ == '__main__':
    # Run local web server
    #run(app, host='localhost', port=8080, debug=False, reloader=True)
    app.run(host='localhost', port=8080, debug=True)
    pass
