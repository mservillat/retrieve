Basic web application to retrieve files from the server

### Deployment (debian on hess-dr.obspm.fr)

Clone repository

    $ cd /opt
    $ git clone https://gitlab.obspm.fr/mservillat/retrieve.git
    $ sudo chown adminluth:adminluth /opt/retrieve
    $ cd /opt/retrieve
    $ sudo mkdir /var/www/retrieve
    $ sudo chown adminluth:adminluth /var/www/retrieve/


Install Miniconda:

    $ wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
    $ sudo bash Miniconda3-latest-Linux-x86_64.sh

Initialize `conda` environment and Flask (2.2.2)

    $ /opt/miniconda3/bin/conda init
    $ conda create --name wsgi3 python==3.10
    $ conda activate wsgi3
    $ conda install flask

Install Gunicorn

    $ pip install gunicorn
    $ cd /opt/retrieve
    $ gunicorn --bind 0.0.0.0:5000 connector:app
    $ sudo cp gunicorn/retrieve.service /etc/systemd/system/
    $ sudo systemctl start retrieve
    $ sudo systemctl enable retrieve
    $ sudo systemctl status retrieve
    $ sudo systemctl restart retrieve

Install Nginx

    $ sudo apt-get install nginx
    $ cd /opt/retrieve
    $ sudo cp nginx/retrieve /etc/nginx/sites-available/
    $ sudo ln -s /etc/nginx/sites-available/retrieve /etc/nginx/sites-enabled
    $ sudo nginx -t
    $ sudo systemctl start nginx
    $ sudo systemctl restart nginx

If needed:

    $ sudo apt remove apache2
    $ sudo apt purge apache2
